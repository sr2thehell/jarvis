<?php
/* Displays all error messages */
//Change made on 25.5.2017 at 9.48 am
session_start();
?>
<!DOCTYPE html>
<html>
<!-- 
    Author Name: Subham Choudhury, Roll-119, CSE- B, 4th year
-->
<head>
  <title>Error</title>
  <?php include 'css/css.html'; ?>
</head>
<body>
<div class="form">
    <h1>Error</h1>
    <p>
    <?php 
	//Showing corresponding message
    if( isset($_SESSION['message']) AND !empty($_SESSION['message']) ): 
        echo $_SESSION['message'];		
    else:
        header( "location: index.php" );
    endif;
	session_destroy();
    ?>
    </p>     
    <a href="index.php"><button class="button button-block"/>Home</button></a>
</div>
</body>
</html>
