-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 25, 2017 at 04:16 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `accounts`
--

-- --------------------------------------------------------

--
-- Table structure for table `hospitals`
--

CREATE TABLE `hospitals` (
  `hid` int(11) NOT NULL,
  `hospital_name` varchar(200) NOT NULL,
  `hospital_contact_info` varchar(12) NOT NULL,
  `hospital_address` varchar(100) NOT NULL,
  `hospital_latitude` double(16,7) NOT NULL,
  `hospital_longitude` double(16,7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hospitals`
--

INSERT INTO `hospitals` (`hid`, `hospital_name`, `hospital_contact_info`, `hospital_address`, `hospital_latitude`, `hospital_longitude`) VALUES
(999, 'Unavailable', 'Unavailable', 'Unavailable', 999.0000000, 999.0000000),
(1001, 'Anandalok Hospital', '03323592931', 'D.K.7/3, Salt Lake City, Kolkata, West Bengal 700091', 22.5847517, 88.4230538),
(1002, 'Apollo Gleneagles Hospital', '03323202122', '58, Canal Circular Road, Kadapara, Kolkata, West Bengal 700054', 22.5748255, 88.4017034),
(1003, 'Columbia Asia Hospital', '03339898969', 'IB 193 Sector III Salt Lake City IB Block, Kolkata, West Bengal 700091', 22.5723289, 88.4127326),
(1004, 'ILS Hospitals, Salt Lake', '03340206500', 'IDD-6, DD 18/6, DD Block, Sector 1, Salt Lake City, Kolkata, West Bengal 700064', 22.5891302, 88.4108605),
(1005, 'R.G. Kar Medical College & Hospital', '03325557656', '1, Kshudiram Bose Sarani, Bidhan Sarani, Kolkata, West Bengal 700004', 22.6046318, 88.3782378),
(1006, 'Central Blood Bank', '03323510619', '205, Vivekananda Rd, Manicktala, Sahitya Parishad, Kolkata, West Bengal 700006', 22.5853659, 88.3751817),
(1007, 'BM Birla Heart Research Hospital', '03330403040', '1, National Library Ave, Sector 1, Alipore, Kolkata, West Bengal 700027', 22.5726460, 88.3638950),
(1008, 'Kothari Medical Centre', '03340127000', '8/3 Alipore Road, Kolkata, West Bengal 700027', 22.5327811, 88.3305109),
(1009, 'Nil Ratan Sircar Medical College and Hospital', '03322653214', 'Sealdah, Raja Bazar, 138, Acharya Jagadish Chandra Bose Rd, Kolkata, West Bengal 700014', 22.5637664, 88.3690089),
(1010, 'Ohio Hospital', '03366026602', 'Plot No. DG-6, Rajarhat, Newtown, DG Block(Newtown), Action Area I, Newtown, Kolkata, West Bengal 70', 22.5779668, 88.4768636),
(1011, 'Peerless Hospital', '03323212621', 'Be386, Bl-be Sector-i, Salt Lake, Salt Lake, BC Block, Sector 1, Salt Lake City, Kolkata, West Benga', 22.5911279, 88.4040215),
(1012, 'SSKM Hospital', '03322041100', '1st floor, Administrative Block, 244 A.J.C. Bose Road, S S K M Hospital, Bhowanipore, Kolkata, West ', 22.5395184, 88.3439568),
(1013, 'Sambhunath Pandit Hospital', '03322870078', '11, Elgin Road, Bhawanipur, Sreepally, Bhowanipore, Kolkata, West Bengal 700020', 22.5383363, 88.3482355),
(1014, 'BelleVue Clinic', '03322872321', '9, Dr. U. N. Brahmachari Street (Formerly Loudon Street), Elgin, Kolkata, West Bengal 700017', 22.5424604, 88.3549171),
(1015, 'Apollo Diagnostic Centre', '03340623245', '336, VIP Rd, Sreebhumi, Lake Town, Kolkata, West Bengal 700048', 22.5995881, 88.4053312),
(1016, 'Calcutta Institute Of Maxillofacial Surgery & Research, Kolkata, West Bengal', '03324412366', '200 Rajdanga, Nabapally, Dindli Basti, Sector A, East Kolkata Twp, Kolkata, West Bengal 700078', 22.5151927, 88.3904839);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hospitals`
--
ALTER TABLE `hospitals`
  ADD PRIMARY KEY (`hid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hospitals`
--
ALTER TABLE `hospitals`
  MODIFY `hid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1017;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
