<!DOCTYPE html>

<?php
//Change made on 25.5.2017 at 9.48 am
/* Displays all successful messages and send emails to donor and requester*/
session_start();
include 'db.php';
include 'php/Notification.php';

$rq_id = $_SESSION['rq_id'];
$don_id = $_SESSION['don_id'];
$hosp_id = $_SESSION['hosp_id'];


    
    // Select requester with matching rq_id
	$result_rq = $mysqli->query("SELECT * FROM requester WHERE id='$rq_id'");
	
	// Select donor with matching don_id
	$result_don = $mysqli->query("SELECT * FROM users WHERE id='$don_id'");
	$row_don=mysqli_fetch_array($result_don);
	
	//Select hospital with matching hosp_id
	$result_hosp = $mysqli->query("SELECT * FROM hospitals WHERE hid='$hosp_id'");
	$row_hosp=mysqli_fetch_array($result_hosp);
	
	
	$notify = new Notification();
	
	//Sending Donor details to requester
	while($row_rq = mysqli_fetch_array($result_rq)){
                        
						//Updating macthed and donor_id attributes of requester
                        $don = $row_don['id'];
						$rqid = $row_rq['id'];
                        $sql = "UPDATE requester SET matched=1  WHERE id='$rqid'";
                        $mysqli->query($sql);
						$sql1 = "UPDATE requester SET donor_id='$don_id'  WHERE id='$rqid'";
                        $mysqli->query($sql1);
						
						
			
                     
		        $to = $row_rq['email'];
				$subject = 'Donor details ( FindMyDonor.com )';
				$message_body = '
				Hello '.$row_rq['first_name'].',

				Your request is accepted by '.$row_don['first_name'].' '.$row_don['last_name'].'

				The donor details are:
				
				Email Id:'. $row_don['email'].'
				
				Contact info:'. $row_don['mobile'].'
				
				Age:'. $row_don['age'].'
				
				Suggested hospital details:
				
				Hospital name:'.$row_hosp['hospital_name'].'
				
				Hospital address:'.$row_hosp['hospital_address'].'
				
				Hospital contact:'.$row_hosp['hospital_contact_info'];
				
                                
                                
				$notify->alertRequester( $to, $subject, $message_body );
				
				
				}
				
	
	
	//Sending requester details to donor
		 $result_rq = $mysqli->query("SELECT * FROM requester WHERE id='$rq_id'");
		 $row_rq=mysqli_fetch_array($result_rq);
		
		 $result_don = $mysqli->query("SELECT * FROM users WHERE id='$don_id'"); 
		 
		 while($row_don=mysqli_fetch_array($result_don)){
				

		        $to = $row_don['email'];
				$subject = 'Request acceptance details ( FindMyDonor.com )';
				$message_body = '
				Hello '.$row_don['first_name'].',

				You have accepted request of '.$row_rq['first_name'].' '.$row_rq['last_name'].'

				The requester details are:
				
				Email Id:'. $row_rq['email'].'
				
				Contact info:'. $row_rq['mobile'].'
				
				Suggested hospital details:
				
				Hospital name:'.$row_hosp['hospital_name'].'
				
				Hospital address:'.$row_hosp['hospital_address'].'
				
				Hospital contact:'.$row_hosp['hospital_contact_info'];

				$notify->alertDonor( $to, $subject, $message_body );
				
				
				}
				
					

    

?>


<html>
<head>
  <title>Thank You</title>
  <?php include 'css/css.html'; ?>
</head>
<body>
<div class="form">
    <h1> 'Thank You for accepting!' </h1>
    <p>
     An Email is sent to you about the requester information.
	 The requester will also get your info by mail!.

    </p>
    <a href="index.php"><button class="button button-block"/>Home</button></a>
</div>
</body>
</html>